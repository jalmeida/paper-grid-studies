import Mixin from '@ember/object/mixin';

let rememberScroll = Mixin.create({

  activate() {
    this._super(...arguments);

    console.log('activate from initializer - mixin')
    window.scrollTo(0,0);
  }
});

export function initialize(/* application */) {
  Ember.Route.reopen(rememberScroll);
}

export default {
  name: 'remember-scroll',
  initialize: initialize
};