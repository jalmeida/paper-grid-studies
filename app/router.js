import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('dashboard', function() {
    this.route('first-level', function() {
      this.route('second-level');
    });
  });
  this.route('admin', function() {});
});

export default Router;
